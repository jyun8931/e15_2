import java.util.*;

public class Student implements Comparable<Student>{

    private String firstName;
    private String lastName;
    private Integer ID;

    public Student(String firstName, String lastName, Integer ID) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.ID = ID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(firstName, student.firstName) &&
                Objects.equals(lastName, student.lastName) &&
                Objects.equals(ID, student.ID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, ID);
    }

    @Override
    public int compareTo(Student o) {
        int lNameComp = this.lastName.compareTo(o.lastName);
        if(lNameComp != 0) return lNameComp;
        int fNameComp = this.firstName.compareTo(o.firstName);
        if(fNameComp != 0) return fNameComp;
        return this.ID.compareTo(o.ID);
    }

    public Integer getID() {
        return ID;
    }

    @Override
    public String toString() {
        return firstName + " " + lastName + " (ID="+ID+"): ";
    }
}