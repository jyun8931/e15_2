import java.io.*;
import java.util.*;

public class E15_2 {

    private static final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    private static TreeMap<Student, String> studentGradeMap;

    public static void main(String[] args) throws Exception {
        studentGradeMap = new TreeMap<>();
        while (true){
            System.out.println("\nSelect an option: \n (a) to add a student \n (r) to remove student \n (m) to modify a grade \n (p) print all grades \n (q) Quit \n");
            char option = br.readLine().charAt(0);
            switch (option) {
                case 'a':
                    record();
                    break;
                case 'r':
                    remove();
                    break;
                case 'm':
                    modifyGrade();
                    break;
                case 'p':
                    display();
                    break;
                case 'q':
                    return;
            }
        }
    }

    private static void record() throws Exception {
        System.out.println("Enter student first name : ");
        String fName = br.readLine();
        System.out.println("Enter student last name : ");
        String lName = br.readLine();
        System.out.println("Enter student ID : ");
        Integer ID = Integer.valueOf(br.readLine());
        System.out.println("Enter student grade : ");
        String grade = br.readLine();
        Student student = new Student(fName, lName, ID);
        studentGradeMap.put(student, grade);
    }

    static void remove() throws Exception {
        System.out.println("Enter ID of student to be removed: ");
        Integer ID = Integer.valueOf(br.readLine());
        Student student = searchStudent(ID);
        if(student == null) System.out.println("No student ID matched with provided ID.");
        else{
            studentGradeMap.remove(student);
            System.out.println("Student with ID : " + ID + " removed.");
        }
    }

    static void modifyGrade() throws Exception {
        System.out.println("Enter ID of student whose grade should be modified: ");
        Integer ID = Integer.valueOf(br.readLine());
        Student student = searchStudent(ID);
        if(student == null) System.out.println("No student ID matched with provided ID.");
        else{
            System.out.println("Enter grade to be replaced : ");
            String grade = br.readLine();
            studentGradeMap.put(student, grade);
            System.out.println("Student with ID : " + ID + " grade modified.");
        }
    }

    private static Student searchStudent(Integer ID){
        Student student = null;
        for(Map.Entry<Student, String> entry : studentGradeMap.entrySet()){
            if(entry.getKey().getID().equals(ID)){
                student = entry.getKey();
                break;
            }
        }
        return student;
    }

    static void display() {
        StringBuilder data = new StringBuilder();
        for(Map.Entry<Student, String> entry : studentGradeMap.entrySet()){
            data.append(entry.getKey().toString()).append(entry.getValue()).append("\n");
        }
        System.out.println(data.toString());
    }
}
